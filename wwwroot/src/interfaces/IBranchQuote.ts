export interface IBranchQuote {
    id:              string;
    parentId:        null;
    options:         Option[];
    quote:           Quote;
    responseStrings: null;
}

export interface Option {
    name:                 string;
    description:          string;
    id:                   string;
    autoCoverages:        Coverage[] | null;
    homeCoverages:        Coverage[] | null;
    type:                 string;
    homePremium:          number | null;
    autoPremium:          number | null;
    homeFees:             any[] | null;
    autoFees:             any[] | null;
    homeSchedule:         any[] | null;
    autoSchedule:         any[] | null;
    homeTotal:            number | null;
    autoTotal:            number | null;
    homeDownPayment:      number | null;
    homeRemainingMonthly: null;
    autoDownPayment:      number | null;
    autoRemainingMonthly: null;
}

export interface Coverage {
    type:      string;
    amount:    number;
    iteration: number;
}

export interface Quote {
    submittedAddress:          CorrectedAddress;
    correctedAddress:          CorrectedAddress;
    priorAddress:              CorrectedAddress;
    fname:                     string;
    lname:                     string;
    rep:                       null;
    email:                     string;
    phone:                     string;
    drivers:                   Driver[];
    cars:                      Car[];
    autoCoverage:              AutoCoverage;
    home:                      Home;
    connectedHome:             null;
    homeCoverage:              HomeCoverage;
    offerings:                 Offerings;
    isHomeOwner:               boolean;
    includeUmbrella:           boolean;
    includeRenters:            boolean;
    rentersCoverage:           RentersCoverage;
    scheduledPersonalProperty: null;
    umbrellaCoverage:          { [key: string]: boolean | null };
    global:                    Global;
}

export interface AutoCoverage {
    policyLimitBIPD:             string;
    policyLimitUMBI:             string;
    policyLimitUIMBI:            string;
    policyLimitPIP:              string;
    policyLimitPIPME:            string;
    policyLimitPIPWL:            string;
    policyLimitPIPACR:           string;
    policyLimitUMPD:             string;
    policyLimitLPD:              string;
    policyLimitIncomeLoss:       string;
    policyLimitAccidentalDeath:  string;
    policyLimitFuneralBenefits:  string;
    policyLimitExtraMedBenefits: string;
    policyLimitTortOption:       string;
    policyLimitComboFBP:         string;
    policyLimitMedicalPayments:  string;
    petInjury:                   boolean;
}

export interface Car {
    vin:                     string;
    year:                    number;
    make:                    string;
    model:                   string;
    primaryUse:              string;
    purchaseDate:            Date;
    garageLocation:          CorrectedAddress;
    deductibleCollision:     string;
    deductibleComprehensive: string;
    limitUMPD:               string;
    limitRental:             string;
    limitACPE:               string;
    limitIncomeLoss:         string;
    roadsideAssistance:      boolean;
    rideSharing:             boolean;
    image:                   null | string;
    aebStatus:               number;
    aebConfirmed:            null;
    antiTheftStatus:         number;
    antiTheftConfirmed:      null;
    blindSpotStatus:         number;
    blindSpotConfirmed:      null;
    coverageLoan:            null;
}

export interface CorrectedAddress {
    address:  string;
    address2: null;
    city:     string;
    state:    string;
    zip:      string;
}

export interface Driver {
    id:                          string;
    isPrimary:                   boolean;
    isCoApplicant:               boolean;
    firstName:                   string;
    middleName:                  null;
    lastName:                    string;
    suffix:                      null;
    gender:                      string;
    maritalStatus:               string;
    highestEducation:            string;
    age:                         number;
    driversLicenseState:         string;
    ageFirstLicensed:            number;
    fullTimeStudent:             boolean;
    goodStudent:                 boolean;
    takenCourse:                 boolean;
    schoolName:                  null;
    schoolZip:                   null;
    addDate:                     Date;
    excludeDriver:               boolean;
    communityDriveParticipation: null;
    postBindMVR:                 null;
    hasUDR:                      boolean;
    nonUDRIncidentCount:         number;
    claimCount:                  number;
    violationCount:              number;
    ratableIncidentCount:        number;
}

export interface Global {
    autoEffectiveDate:         Date;
    homeEffectiveDate:         Date;
    rateControlDate:           Date;
    discountInventoryScore:    boolean;
    discountDriverScore:       boolean;
    discountPaperless:         boolean;
    homeownersPaymentType:     string;
    homeownersPaymentMethod:   string;
    autoPaymentType:           string;
    autoPaymentMethod:         string;
    currentAutoCarrier:        string;
    currentAutoCarrierEndDate: null;
    autoBillingDayOfMonth:     number;
    homeBillingDayOfMonth:     number;
    priorIndividualBILimit:    number;
    priorOccurrenceBILimit:    number;
}

export interface Home {
    basementSqFt:            number;
    basementType:            string;
    constructionType:        string;
    exteriorWallType:        string;
    garageCarCapacity:       number;
    garageType:              string;
    homeLocation:            CorrectedAddress;
    homeQuality:             string;
    mortgageDetails:         any[];
    numFamiliesInDwelling:   number;
    numFireplaces:           number;
    numWoodBurningStoves:    number;
    numFullBathrooms:        number;
    numHalfBathrooms:        number;
    numOccupants:            number;
    numStories:              number;
    ownershipStatus:         string;
    purchaseDate:            Date;
    roofOriginal:            boolean;
    roofShape:               string;
    roofType:                string;
    roofYear:                number;
    sqFt:                    number;
    typeOfHome:              string;
    yearBuilt:               number;
    latitude:                number;
    longitude:               number;
    fpc:                     string;
    county:                  string;
    fireHydrantWithin1000ft: null;
    stormShutters:           boolean;
    homeClaimCount:          number;
}

export interface HomeCoverage {
    coverageA:                      number;
    coverageIBSC:                   number;
    coverageBCPctOfA:               number;
    coverageBPctOfA:                number;
    coverageCPctOfA:                number;
    coverageDPctOfA:                number;
    coverageX:                      number;
    coverageY:                      number;
    coverageWaterBackup:            number;
    coverageBuildingMaterialsTheft: boolean;
    coverageCourseOfConstruction:   boolean;
    coverageExtendedPremises:       boolean;
    coverageFairRental:             boolean;
    coverageRSExtended:             boolean;
    coverageYardAndGarden:          boolean;
    coverageBP:                     number;
    coverageCA:                     number;
    coverageDR:                     boolean;
    coverageF:                      number;
    coverageG:                      number;
    coverageJ:                      number;
    coverageMI:                     number;
    coverageMS:                     boolean;
    coverageMSALE:                  boolean;
    coverageSP:                     number;
    coverageST:                     number;
    coverageEBSL:                   boolean;
    homeownerProtection:            boolean;
    deductibleAllOther:             number;
    deductibleHurricane:            number;
    deductibleWindHail:             number;
    windHailExclusion:              boolean;
    minimumDeductible:              number;
    minimumDeductibleValue:         number;
}

export interface Offerings {
    autoRejectCode:         null;
    monolineAutoRejectCode: null;
    homeownersRejectCode:   null;
    offerAuto:              boolean;
    offerHomeowners:        boolean;
    offerRenters:           boolean;
    offerUmbrella:          boolean;
    offerWaterBackup:       boolean;
    priorCustomer:          boolean;
}

export interface RentersCoverage {
    rentersLocation: CorrectedAddress;
    coverageCD:      string;
    coverageX:       number;
    coverageY:       number;
    deductible:      number;
}
