import{c as t,a as r,_ as e,m as s}from"./mitt.js";import{x as a,y as i,z as m}from"./vue-axios.esm.min.js";import{_ as p}from"./App.vue_vue_type_style_index_0_lang.js";import{F as n,E as c}from"./vee-validate.esm.js";const _=document.getElementById("app"),o=a(p),d=[{path:"/",component:()=>e(()=>import("./QuotePage.js"),__vite__mapDeps([0,1,2,3,4]))},{path:"/bind",component:()=>e(()=>import("./BindForm.js"),__vite__mapDeps([5,3]))}],u=t({history:r(),routes:d}),l=s();o.component("Field",n);o.component("ErrorMessage",c);o.use(i,m);o.use(u);o.provide("axios",o.config.globalProperties.axios);o.provide("emitter",l);o.mount(_);
function __vite__mapDeps(indexes) {
  if (!__vite__mapDeps.viteFileDeps) {
    __vite__mapDeps.viteFileDeps = ["QuotePage.js","vue-axios.esm.min.js","vee-validate.esm.js","_plugin-vue_export-helper.js","QuotePage.css","BindForm.js"]
  }
  return indexes.map((i) => __vite__mapDeps.viteFileDeps[i])
}
