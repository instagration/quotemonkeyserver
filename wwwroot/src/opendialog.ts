import { createApp } from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import AppComponent from './views/OpenDialog/App.vue'
import {createRouter, createWebHashHistory} from "vue-router"
import {ErrorMessage, Field} from "vee-validate";
import mitt from "mitt";

const el = document.getElementById('app');

const app = createApp(AppComponent)

const routes = [
    { path: '/', component: () => import('./views/OpenDialog/OpenDialogChat.vue') },
]

const router = createRouter({
    // 4. Provide the history implementation to use. We
    // are using the hash history for simplicity here.
    history: createWebHashHistory(),
    routes, // short for `routes: routes`
})

const emitter = mitt()

app.component("Field", Field)
app.component("ErrorMessage", ErrorMessage)

app.use(VueAxios, axios);
app.use(router)

app.provide('axios', app.config.globalProperties.axios)
app.provide('emitter', emitter)

app.mount(el as HTMLElement);
