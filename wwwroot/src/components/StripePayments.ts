import {
    PaymentIntent,
    Stripe,
    StripeCardElement,
    StripeElements, StripeElementsOptionsMode,
} from "@stripe/stripe-js";


export class StripePayments {
    stripe: Stripe
    paymentElement: StripeCardElement | undefined;
    clientSecret: string | undefined;
    elements: StripeElements | undefined;

    successfulPaymentHandler: null | ((token: string, paymentIntent: PaymentIntent) => void) = null;

    constructor(stripe: Stripe) {
        this.stripe = stripe;
    }

    async initPaymentForm(paymentAmount: number) {
        if (paymentAmount == 0) {
            debugger;
        }
        const response = await fetch(`/BranchTest/CreatePaymentIntent?price=${paymentAmount}`, {
            method: "POST",
            headers: {"Content-Type": "application/json"},
        });

        let clientSecret = this.clientSecret = (await response.json()).clientSecret;
        const appearance = {theme: 'stripe',}
        this.elements = this.stripe.elements({appearance, clientSecret} as StripeElementsOptionsMode);
        if (!this.elements) return;

        const paymentElementOptions = {
            layout: "tabs",
            business: {name: "Garzella Group"}

        };
// @ts-ignore
        this.paymentElement = this.elements.create("card", paymentElementOptions);

        this.paymentElement.mount("#payment-element");
    }

    async handleSubmit(e: Event) {
        e.preventDefault();
        this.setLoading(true);
        if (!this.stripe || !this.paymentElement || !this.clientSecret) return;

        const tokenResponse = await this.stripe.createToken(this.paymentElement)
        const token = tokenResponse?.token?.id;

        // const result = await stripe.confirmPayment({
        //   elements,
        //   confirmParams: {
        //     // Make sure to change this to your payment completion page
        //     return_url: "",
        //   },
        //   redirect: "if_required"
        // });

        const result = await this.stripe.confirmCardPayment(
            this.clientSecret,
            {payment_method: {card: this.paymentElement}}
        )
        const {error} = result;

        if (!error && token) {
            if (this.successfulPaymentHandler) {
                this.successfulPaymentHandler(token, result.paymentIntent)
            }

            return
        }

        if (error) {
            if ((error.type === "card_error" || error.type === "validation_error") && (error?.message)) {
                this.showMessage(error.message);
            } else {
                this.showMessage("An unexpected error occurred.");
            }

            this.setLoading(false);
        }
    }


    async checkStatus() {
        if (!this.stripe) return;

        const clientSecret = new URLSearchParams(window.location.search).get(
            "payment_intent_client_secret"
        );

        if (!clientSecret) {
            return;
        }

        const {paymentIntent} = await this.stripe.retrievePaymentIntent(clientSecret);
        if (!paymentIntent) return;

        switch (paymentIntent.status) {
            case "succeeded":
                this.showMessage("Payment succeeded!");
                break;
            case "processing":
                this.showMessage("Your payment is processing.");
                break;
            case "requires_payment_method":
                this.showMessage("Your payment was not successful, please try again.");
                break;
            default:
                this.showMessage("Something went wrong.");
                break;
        }
    }

    showMessage(messageText: string) {
        const messageContainer = document.querySelector("#payment-message");
        if (!messageContainer) return;

        messageContainer.classList.remove("hidden");
        messageContainer.textContent = messageText;

        setTimeout(function () {
            messageContainer.classList.add("hidden");
            messageContainer.textContent = "";
        }, 4000);
    }

    setLoading(isLoading: boolean) {
        if (isLoading) {
            // @ts-ignore
            document.querySelector("#submit").disabled = true;
            // @ts-ignore
            document.querySelector("#spinner").classList.remove("hidden");
            // @ts-ignore
            document.querySelector("#button-text").classList.add("hidden");
        } else {
            // @ts-ignore
            document.querySelector("#submit").disabled = false;
            // @ts-ignore
            document.querySelector("#spinner").classList.add("hidden");
            // @ts-ignore
            document.querySelector("#button-text").classList.remove("hidden");
        }
    }
}