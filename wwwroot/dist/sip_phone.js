import{c as o,a as e,_ as r,m as s}from"./mitt.js";import{x as a,y as i,z as m}from"./vue-axios.esm.min.js";import{_ as p}from"./App.vue_vue_type_style_index_0_lang.js";const c=document.getElementById("app"),t=a(p),n=[{path:"/",component:()=>r(()=>import("./SipPhoneIndex.js"),__vite__mapDeps([0,1]))}],_=o({history:e(),routes:n}),u=s();t.use(i,m);t.use(_);t.provide("axios",t.config.globalProperties.axios);t.provide("emitter",u);t.mount(c);
function __vite__mapDeps(indexes) {
  if (!__vite__mapDeps.viteFileDeps) {
    __vite__mapDeps.viteFileDeps = ["SipPhoneIndex.js","vue-axios.esm.min.js"]
  }
  return indexes.map((i) => __vite__mapDeps.viteFileDeps[i])
}
