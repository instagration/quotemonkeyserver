﻿import { createApp } from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

import AppComponent from './views/App.vue'
import TextBoxInput from './views/TextBoxInputComponent.vue'
import TextBlock from './views/TextBlockComponent.vue'
import AccountAdditionalInterest from './views/AccountAdditionalInterestComponent.vue'
import StackPanel from './views/StackPanelComponent.vue'
import GroupBox from './views/GroupBoxComponent.vue'
import AccountSourcesCombobox from './views/AccountSourcesComboboxComponent.vue'
import AccountContactsControl from './views/AccountContactsComboboxComponent.vue'
import ComboboxInput from './views/ComboboxInputComponent.vue'
import DatePickerInput from './views/DatePickerInputComponent.vue'
import AccountAddressControl from './views/AccountAddressControlComponent.vue'
import PersonalLeadPolicyLines from './views/PersonalLeadPolicyLinesComponent.vue'
import CheckboxInput from './views/CheckboxComponent.vue'
import BusinessPolicyLine from './views/AddLeadViews/Partials/BusinessPolicyLine.vue'

const el = document.getElementById('app');

const app = createApp(AppComponent)

app.component('TextBoxInput', TextBoxInput)
app.component('TextBlock', TextBlock)
app.component('AccountAdditionalInterest', AccountAdditionalInterest)
app.component('StackPanel', StackPanel)
app.component('GroupBox', GroupBox)
app.component('AccountSourcesCombobox', AccountSourcesCombobox)
app.component('AccountContactsControl', AccountContactsControl)
app.component('DatePickerInput', DatePickerInput)
app.component('AccountAddressControl', AccountAddressControl)
app.component('PersonalLeadPolicyLines', PersonalLeadPolicyLines)
app.component('CheckboxInput', CheckboxInput)
app.component('ComboboxInput', ComboboxInput)
app.component('BusinessPolicyLine', BusinessPolicyLine)

app.use(VueAxios, axios);
app.provide('axios', app.config.globalProperties.axios)

app.mount(el as HTMLElement);