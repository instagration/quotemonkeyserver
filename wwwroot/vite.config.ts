import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
/** @type {import('vite').UserConfig} */
export default defineConfig({
  plugins: [vue()],
  build: {
    rollupOptions: {
      input: {
        "main": "./src/index.ts",
        "branch_quotes": "./src/branch_quotes.ts",
        "sip_phone": "./src/sip_phone.ts",
        "opendialog": "./src/opendialog.ts",
      },
      output: {
        entryFileNames: `[name].js`,
        chunkFileNames: `[name].js`,
        assetFileNames: `[name].[ext]`
      }
    },
  }
})
