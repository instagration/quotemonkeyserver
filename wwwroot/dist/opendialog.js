import{c as t,a as r,_ as s,m as n}from"./mitt.js";import{r as a,o as c,g as i,x as p,y as m,z as _}from"./vue-axios.esm.min.js";import{_ as u}from"./_plugin-vue_export-helper.js";import{F as l,E as d}from"./vee-validate.esm.js";const f={};function g(A,B){const e=a("router-view");return c(),i(e)}const v=u(f,[["render",g]]),x=document.getElementById("app"),o=p(v),E=[{path:"/",component:()=>s(()=>import("./OpenDialogChat.js"),__vite__mapDeps([0,1,2,3]))}],h=t({history:r(),routes:E}),y=n();o.component("Field",l);o.component("ErrorMessage",d);o.use(m,_);o.use(h);o.provide("axios",o.config.globalProperties.axios);o.provide("emitter",y);o.mount(x);
function __vite__mapDeps(indexes) {
  if (!__vite__mapDeps.viteFileDeps) {
    __vite__mapDeps.viteFileDeps = ["OpenDialogChat.js","vue-axios.esm.min.js","_plugin-vue_export-helper.js","OpenDialogChat.css"]
  }
  return indexes.map((i) => __vite__mapDeps.viteFileDeps[i])
}
