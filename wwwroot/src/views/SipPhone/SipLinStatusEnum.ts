export enum SipLinStatusEnum {
    Ready,
    CallTrying,
    CallRinging,
    CallFailed,
    CallAnswered,
}